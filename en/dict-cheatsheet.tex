\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}
\usepackage{cheatsheet}

%opening
\title{Python3 Datatypes Cheatsheet}
\subtitle{Dictionaries}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}
\label{sec.dict.definition}
A dictionary is an associative array with key-value pairs.
%  where a \emph{key} is assigned to a \emph{value}.


\begin{enumerate}
\item The key can be any object that uses the methods
\pymagic{hash}() and \pymagic{eq}().

\item Each key must be \emph{hashable}.
This is the case if it has a hash value and this value is
constant at runtime. Data types that change their contents
at runtime, such as lists, dictionaries, etc., are therefore
not suitable as keys.

\item Since Python 3.6, Dict are ordered!
\end{enumerate}

\section{Creating Dictionaries}
Dicts can be created in different ways:

\subsection{Using \pyth{dict()}}
With the function \pyth{dict()}, keys and values can be
specified in the call:

\begin{python}
>>> d1 = dict(one=1, two=2)
\end{python}

Attention: This only works if there are no
Python keywords specified as keys!
The following call results in a \pyth{SyntaxError}
due to the \pyth{class} keyword:

\begin{python}
>>> d2 = dict(class="Version")
              ^
SyntaxError: invalid syntax
\end{python}


\subsection{Using Curly Brackets Notation}
% Use curly brackets to create a dict. Separate keys from
% values by a colon:

\begin{python}
>>> d2 = {'one': 1, 'two': 2)
\end{python}

There are no restrictions here (except the ones in
section~\ref{sec.dict.definition}). So this call is possible:

\begin{python}
>>> d2 = {'class': 'Version'}
\end{python}

\subsection{Joining Separate Keys and Values with \pyth{zip()}}
If keys and values exist separately, \pyth{zip()} joins them together:

\begin{python}
>>> keys = ['one', 'two', 'three']
>>> values = [1, 2, 3]
>>> dict(zip(keys, values))
{'one': 1, 'three': 3, 'two': 2}
\end{python}

If one of the above lists is of different length, the shortest list length is used.

If you want to specify a fill value for lists of different lengths,
use the \pyth{zip_longest()} function from the \pyth{itertools} package:

\begin{python}
>>> from itertools import zip_longest
>>> keys = ['one', 'two', 'three', 'four']
>>> values = [1, 2, 3]
>>> dict(zip_longest(keys, values,
                     fillvalue='n/a'))
{'four': 'n/a', 'one': 1, 'three': 3, 'two': 2}
\end{python}


\subsection{Using a List/Tuple of Key-Value Pairs}
If key-value pairs exist in a list or tuple,
\pyth{dict} can convert them directly into a dictionary:

\begin{python}
>>> alist = [('one', 1), ('two', 2)]
>>> dict(alist)
{'one': 1, 'two': 2}
\end{python}


\subsection{Using an Existing Dict}
A new dict can also be created from an existing dict,
including overwriting values:

\begin{python}
>>> d = dict(one=1, two=2)
>>> dict(d, two="hi!", three=3)
{'one': 1, 'three': 3, 'two': 'hi!'}
\end{python}

See also the method \pyth{dict.update()}:

\begin{python}
>>> d = dict(one=1, two=2)
>>> other = dict(three=3, four=4, one="eins")
>>> d.update(other)
>>> d
{'four': 4, 'one': 'eins', 'three': 3, 'two': 2}
\end{python}


\subsection{Creating a Dict Through Dict Comprehension}

\begin{python}
>>> name = ["Tux", "Wilber"]
>>> task = ("Linux", "Gimp")
>>> {key: value for key, value in zip(name, task)}
{'Tux': 'Linux', 'Wilber': 'Gimp'}
\end{python}

\subsection{Merging Dicts}
Available only for Python 3.5 and above. Takes two dicts together. The order
is important:

\begin{python}
>>> a = dict(one=1, two=2)
>>> b = dict(zero=0, one=10, three=3)
>>> {**a, **b}
{'one': 10, 'three': 3, 'two': 2, 'zero': 0}
>>> {**b, **a}
{'one': 1, 'three': 3, 'two': 2, 'zero': 0}
\end{python}

\subsection{Using the \pyth{dict.fromkeys} Method}
The method \pyth{dict.fromkeys} expects an iterable and returns a new dict.
By default, the value is filled with \pyth{None}; however, if a fill value
is specified, it is used:

\begin{python}
>>> dict.fromkeys('hello') # <- Fuellwert None
{'e': None, 'h': None, 'l': None, 'o': None}
>>> dict.fromkeys('hello', '')
{'e': '', 'h': '', 'l': '', 'o': ''}
\end{python}

% -------------------
\section{Unpacking Dictionaries}
Function arguments can be collected in a dict and passed to
the function by \emph{dict unpacking}:

\begin{python}
>>> def func(a, b, c):
...     return a * b + c
>>> d = dict(a=2, b=4, c=3)
>>> func(**d)  # <- Unpacking
11
\end{python}

If not all arguments are passed as keys, you get:

\begin{python}
>>> d = dict(a=2, b=4)  # 'c' is missing
>>> func(**d)
...
TypeError: func() missing 1 required positional argument: 'c'
\end{python}

% -------------------
\section{Available Operations}

\subsection{Testing for Truth Value}

\begin{python}
>>> bool(dict())
False
>>> bool(dict(one=1))
True
\end{python}


\subsection{Getting Number of Entries with \pyth{len()}}
\begin{python}
>>> len(dict(one=1, two=2))
2
\end{python}


\subsection{Comparing two Dicts}
In a comparison, dicts compare keys as well as values:

\begin{python}
>>> d1 = dict(one=1, two=2)
>>> d2 = dict(one=1, two=2)
>>> d3 = dict(one=1, two='2')
>>> d1 == d2
True
>>> d1 == d3
False
\end{python}


\subsection{Getting a Value}
If the value exists for a key, the value is returned,
otherwise a \pyth{KeyError} is raised:

\begin{python}
>>> d = dict(one=1, two=2)
>>> d['one']
1
>>> d['ten']
KeyError: 'ten'
\end{python}

With the method \pyth{dict.get()} a fallback value can be returned
if the key is not found:

\begin{python}
>>> d.get('ten', 10)
10
\end{python}


\subsection{Getting and Removing a Specific Value}
The method \pyth{dict.pop()} removes the specified key and returns
the corresponding value:

\begin{python}
>>> d = dict(one=1, two=2)
>>> d.pop('two')
2
\end{python}

If the key is not present in the dict, a \pyth{KeyError} is raised.
To avoid the exception, you can add a fallback value as a second
argument to the \pyth{dict.pop()} method:

\begin{python}
>>> d = dict(one=1, two=2)
>>> d.pop('three')
KeyError: 'three'
>>> d.pop('three', "fixme")
'fixme'
\end{python}

\subsection{Getting and Removing as Key-Value Tuples}
The method \pyth{dict.popitem()} remove and return a key-value pair.
Pairs are return as last-in, first-out order:

\begin{python}
>>> d = dict(one=1, two=2)
>>> d.popitem()
('two', 2)
>>> d
{'one': 1}
>>> d.popitem()
('one', 1)
\end{python}


\subsection{Amend Existing Dictionary with Another Key-Value Pair}
A new key-value pair is made by an assignment.
If the key already exists, the existing value is overwritten with the new value:

\begin{python}
>>> d = dict(one=1, two=2)
>>> d['tree'] = 3
>>> d['one'] = "eins"
>>> d
{'one': 'eins', 'tree': 3, 'two': 2}
\end{python}


\subsection{Conditional Assigning of a Key}
The method \pyth{dict.setdefault()} inserts a key with a value
if the key is not in the dict. If the key is in the dictionary,
return the value for the key, otherwise return the default:

\begin{python}
>>> d = dict(one=1, two=2)
>>> d.setdefault('three', 3)
3
>>> d.setdefault('two', 'n/a')
2
\end{python}


\subsection{Removing a Key}
Deleting only works if the key is present.
If not, a \pyth{KeyError} is raised:

\begin{python}
>>> d = dict(one=1, two=2)
>>> del d['two']
>>> del d['ten']
KeyError: 'ten'
\end{python}


\subsection{Checking for Presence/Absense of a Key}
\begin{python}
>>> d = dict(one=1, two=2)
>>> 'three' in d
False
>>> 'three' not in d
True
\end{python}


% -------------------
\section{Iterating over a Dict}
The objects returned by \pyth{dict.keys()}, \pyth{dict.values()},
and \pyth{dict.items()} are iterable \emph{view objects}.

View objects support a dynamic view of the original dict,
i.~e.\ if the original dict changes, the view object changes as well.

\subsection{Iterating Through Keys Directly}
By default, iterating over a dict always gets back the key:

\begin{python}
>>> for key in dict(one=1, two=2):
...    print(key)
one
two
\end{python}

The method \pyth{dict.keys()} gives the same result:

\begin{python}
>>> for key in dict(one=1, two=2).keys():
...    print(key)
one
two
\end{python}


\subsection{Iterating Over Values}
With the method \pyth{dict.values()} you get the values only:

\begin{python}
>>> for value in dict(one=1, two=2).values():
...    print(value)
1
2
\end{python}

\subsection{Iterating over Key-Value Pairs}
The method \pyth{dict.items()} returns a key-value pair as a tuple:

\begin{python}
>>> for item in dict(one=1, two=2).items():
...    print(item)
('one', 1)
('two', 2)
\end{python}


% -------------------
\section{Other Dict-Like Objects}

\subsection{\pyth{collections.Chainmap}}
A \pyth{ChainMap} object groups multiple dicts or other mapping objects
to create a single, updatable view object.

A \pyth{ChainMap} points to the mapping objects by reference.
If one of the mappings passed to the \pyth{ChainMap} changes,
the \pyth{ChainMap} will change as well:

\begin{python}
>>> from collections import ChainMap
>>> defaults = {'color': 'red', 'USER': 'guest',
                'flag': False}
>>> cliargs = {'color': 'blue',  'USER': 'tux'}
>>> combinded = ChainMap(cliargs, defaults)
>>> dict(combinded)
{'USER': 'tux', 'color': 'blue', 'flag': False}
>>> cliargs['USER'] = 'wilber'
>>> combinded['USER']
'wilber'
\end{python}


\subsection{\pyth{collections.defaultdict}}
The main functionality is the same as with \pyth{dict}.
The only difference is that the first argument must be
a callable (a so-called \emph{factory function}) or \pyth{None}
(the default value).

The factory function is used to specify what should be returned
if a key cannot be found:

\begin{python}
>>> from collections import defaultdict
>>> dd = defaultdict(str, user="Tux")
>>> dd['password']
''
>>> # auch moeglich:
>>> dd = defaultdict(lambda: "n/a", user="Tux")
>>> dd['password']
'n/a'
\end{python}


\subsection{\pyth{collections.OrderedDict}}
The main functionality is the same as with \pyth{dict}.
However, the order of the keys remains the same.
If a new entry has a overwrites an existing entry,
the position remains unchanged:

\begin{python}
>>> from collections import OrderedDict
>>> d1 = OrderedDict([('a',1), ('b',2), ('c',3)])
>>> d2 = OrderedDict([('b',2), ('c',3), ('a',1)])
>>> d1 == d2
False
>>> list(d1)
['a', 'b', 'c']
>>> list(d2)
['b', 'c', 'a']
\end{python}

With Python >= 3.6, ordering is already available in the built-in
datatype dict.

\end{multicols}
\end{document}
\maketitle
