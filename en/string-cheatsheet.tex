\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage{babel}
\usepackage{cheatsheet}

%opening
\title{Python3 Datatypes Cheatsheet}
\subtitle{Strings}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}
\label{sec.dict.definition}
Strings are an immutable sequence of Unicode code points.

\section{Creating Strings}
Strings are created using \pyth{str} or directly using quotation marks:

\begin{itemize}
\item in single quotes: \pyth{'hello'}
\item in double quotes: \pyth{"hello"}
\item in triple quotes: \pyth{"""hello"""} or \pyth{''hello''}
\end{itemize}

Long or unwieldy strings can be split on different lines:

\begin{itemize}
\item With a backslash:

\begin{python}
hello = "hello " \
        "world"
\end{python}

\item With a tuple (without(!) a backslash):
\begin{python}
hello = ("hello "
         "world")
\end{python}
\end{itemize}

Both creates the string \pyth{Hello World}.


\section{Using a Prefix for Strings}
A prefix before the quotation marks can be used to control certain properties:

\begin{description}
\item[\texttt{b} Bytes Literals]
    generate an unchangeable sequence of single bytes, i.e. integers in the
    range of 0...256. The content can also be represented by ASCII characters.
    Characters above the numeric value of 128 must be masked by an appropriate
    escape sequence.

\item[\texttt{u} Unicode Strings]
    this is the default value and does not need to be specified explicitly.
    Only available for compatibility reasons.

\item[\texttt{r} Raw Strings]
    backslashes are interpreted as normal characters.

\item[\texttt{f} f-String (Python \textgreater{}3.6)]
    is a so-called \emph{formatted string literal}. The content contains a
    placeholder in curly brackets which is evaluated at runtime:

\begin{python}
>>> name = "Tux"
>>> f"My name is {name}."
'My name is Tux.'
\end{python}
\end{description}

\section{Escape Sequences}
If no \texttt{r} or \texttt{R} prefix is present, escape sequences in
string and byte literals are interpreted similar to standard C.
The following list gives an overview. The complete list is available
at \cite{python.literals}.

\begin{description}
\item[\texttt{\char`\\t}] ASCII horizontal tab (TAB)
\item[\texttt{\char`\\n}] ASCII linefeed (LF)
\item[\texttt{\char`\\"}] double quote
\item[\texttt{\char`\\\textquotesingle}] single quote
\item[\texttt{\char`\\u\{xxxx\}}] Unicode character
    as 16-bit value (hexadecimal)
\item[\texttt{\char`\\U\{xxxxxxxx\}}] Unicode character
    as 32-bit value (hexadecimal)
\item[\texttt{\char`\\N\{<NAME>\}}]
% \texttt{\textbackslash N$\lbrace$NAME$\rbrace$}]
    Named character from the Unicode database (case is irrelevant)
\end{description}

\begin{python}
>>> 'Hello \'Tux\'!'
"Hello 'Tux'!"
>>> s1 = "\\textbf{Hello TeX}"
>>> s2 = r"\textbf{Hello TeX}"
>>> s1 == s2
True
>>> "Euro: \N{euro sign} oder \u20ac"
\end{python}
% 'Euro: € oder €'

\section{String Methods}

\subsection{Searching and Replacing}
\pyfunc{str.find(sub[, start[, end]])}\par
\pyfunc{str.index(sub[, start[, end]])}\par
\pyfunc{str.replace(old, new[, count])}\par
\pyfunc{str.rfind(sub[, start[, end]])}\par
\pyfunc{str.rindex(sub[, start[, end]])}

\begin{python}
>>> "Hi Python!".find("Py") # wie .index("Py")
3
>>> "Hi Python!".find("py")
-1
>>> "Hi Python!".index("py")
ValueError: substring not found
>>> "Hi Python!".replace("Hi", "Hello")
'Hello Python!'
>>> "Hi! Python!".rfind("!")
10
\end{python}

\subsection{Splitting Strings}

\pyfunc{str.splitlines([keepends])}\par
\pyfunc{str.split(sep=None, maxsplit=-1)}\par
\pyfunc{str.rsplit(sep=None, maxsplit=-1)}\par
\pyfunc{str.rpartition(sep)}

\begin{python}
>>> '1,2,3'.split(',')
['1', '2', '3']
>>> '1,2,3'.split(',', maxsplit=1)
['1', '2,3']
>>> '1,2,,3,'.split(',')
['1', '2', '', '3', '']
>>> "Hello\nWorld\n".splitlines()
['Hello', 'World']
>>> "Hello\nWorld\n".splitlines(True)
['Hello\n', 'World\n']
>>> "Hello\nWorld\n".partition('\n')
('Hello', '\n', 'World\n')
>>> "Hello\nWorld\n".rpartition('\n')
('Hello\nWorld', '\n', '')
\end{python}

\subsection{Concatenating and Combining Strings}
The fastest way is to have a list of strings and the method \pyth{str.join}:

\begin{python}
>>> " ".join(["Hello", "World"])
'Hello World'
>>> "--".join(["Hello", "World"])
'Hello--World'
>>> "Hello" + " " + "World"
'Hello World'
\end{python}

If you need to combine a few strings, the plus operator works also well:

\begin{python}
>>> "Hello" + " " + "World"
'Hello World'
\end{python}

Sometimes it is easier to use f-strings in case you have lots of punctuation:

\begin{python}
>>> name="Tux"
>>> place="Greenland"
>>> f"Hello {name}. Are you coming from {place}?"
'Hello Tux. Are you coming from Greenland?'
\end{python}

\subsection{Aligning and Filling Strings}

\pyfunc{str.center(width[, fillchar])}\par
\pyfunc{str.ljust(width[, fillchar])}\par
\pyfunc{str.expandtabs(tabsize=8)}\par
\pyfunc{str.rjust(width[, fillchar])}\par
\pyfunc{str.zfill(width)}

\begin{python}
>>> "Tux".center(5)
' Tux '
>>> "Tux".center(5, '-')
'-Tux-'
>>> '1\t2'.expandtabs()
'1       2'
>>> '1\t2'.expandtabs(4)
'1   2'
\end{python}

\iffalse
\subsection{Transforming Strings}

\pyfunc{str.capitalize()}\par
\pyfunc{str.casefold()}\par
\pyfunc{str.lower()}\par
\pyfunc{str.maketrans(x[, y[, z]])}\par
\pyfunc{str.swapcase()}\par
\pyfunc{str.translate(table)}\par
\pyfunc{str.title()}\par
\pyfunc{str.upper()}
\fi

\iffalse
\begin{python}
>>> "Hello Fuß".capitalize()
'Hello fuß'
>>> "Hello Fuß".casefold()
'hello fuss'
>>> "Hello Fuß".lower()
'hello fuß'
>>> "Hello Fuß".swapcase()
'hELLO fUSS'
>>> "Hello fuß".title()
'Hello Fuß'
>>> "Hello Fuß".upper()
'HELLO FUSS'
\end{python}
\fi

\iffalse
\subsection{Formatting Strings}

\pyfunc{str.format(*args, **kwargs)}\par
\pyfunc{str.format\_map(mapping)}


\begin{python}
>>> "{} {}".format("Tux", "Penguin")
'Vorname: Tux, Nachname: Penguin'
>>> "{vname} {nname}".format(nname="Penguin",
                             vname="Tux")
'Vorname: Tux, Nachname: Penguin'
\end{python}


For more details, see section~\ref{sec.custom.string} and
\url{https://docs.python.org/3.6/library/string.html#formatstrings}.
\fi

\subsection{Encoding and Decoding Strings}
Python3 distinguishes between strings (in Unicode) and byte strings.
To convert one into the other, the encoding must be specified:

\iffalse
% Die Standardkodierung ist über \pyth{sys.getfilesystemencoding()} abrufbar (meist UTF-8).
The difference becomes obvious when a character has a Unicode code
point greater than 256.
The Euro character (€ = U+20AC) as a Unicode string is seen as one
character, while as a byte string it has a sequence of three characters
(encoded as UTF-8):

\begin{python}
>>> s1 = '\N{euro sign}' # U+20AC
>>> b1 = bytes(s1, 'UTF-8')
b'12 \xe2\x82\xac'
>>> len(s), len(b)
(1, 3)
# Oder:
>>> b2 = s1.encode('utf-8')
>>> b1 == b2
True
\end{python}
\fi


\begin{description}
\item[From Unicode String to Byte String]
    using the function \pyth{byte(uni_string, encoding)} or with the
    method \pyth{str.encode()}:

\begin{python}
>>> s1 = '\N{euro sign}' # U+20AC
>>> bytes(s1, 'utf-8') == s1.encode('utf-8')
True
\end{python}

\item[From Byte String to Unicode String]
    using the function \pyth{str(byte_string, encoding)} or with 
    the method \pyth{bytes.decode()}:

\begin{python}
# Euro als UTF-8 Stream
>>> b1 = b'\xe2\x82\xac'
>>> str(b1, 'utf-8') == b1.decode('utf-8')
True
\end{python}
\end{description}

If errors occur during the conversion, the exceptions
\pyth{UnicodeDecodeError} or \pyth{UnicodeEncodeError} are raised.


\subsection{Checking Start und End of a String}

\pyfunc{str.endswith(suffix[, start[, end]])}\par
\pyfunc{str.startswith(prefix[, start[, end]])}

\begin{python}
>>> "Python is great".startswith("Python")
True
>>> "Python is great".startswith(("Py", "XML"))
True
>>> "Python is great".startswith("Python", 6)
False
\end{python}

\subsection{Checking String Properties}
The methods all start with \pyth{str.isNAME()} and return \pyth{True}
if the condition is true, otherwise \pyth{False}:\linebreak
\pyfunc{str.isalnum()}, \pyfunc{str.isalpha()}, \pyfunc{str.isdecimal()},
\pyfunc{str.isdigit()}, \pyfunc{str.isidentifier()},
\pyfunc{str.islower()}, \pyfunc{str.isnumeric()},
\pyfunc{str.isprintable()}, \pyfunc{str.isspace()},
\pyfunc{str.istitle()}, \pyfunc{str.isupper()}.

\iffalse
\begin{description}
\item[\pyfunc{str.isalnum()}] Gibt \pyth{True} zurück wenn alle Zeichen
 alphanumerisch sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isalpha()}] Gibt \pyth{True} zurück wenn alle Zeichen
alphabetisch sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isdecimal()}] Gibt \pyth{True} zurück wenn alle Zeichen
Dezimalzeichen sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isdigit()}] Gibt \pyth{True} zurück wenn alle Zeichen
Zahlen sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isidentifier()}] Gibt \pyth{True} zurück wenn der
String ein gültiger Bezeichner ist, siehe \url{https://docs.python.org/3/eference/lexical_analysis.html#identifiers}.

\item[\pyfunc{str.islower()}] Gibt \pyth{True} zurück wenn alle
\emph{cased characters} im String Kleinbuchstaben sind, anderfalls
\pyth{False}.

\item[\pyfunc{str.isnumeric()}] Gibt \pyth{True} zurück wenn alle
Zeichen im String nummerisch sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isprintable()}] Gibt \pyth{True} zurück wenn der String
nur Zeichen enthält, die im Unicode-Standard als \emph{Other} oder
\emph{Separator} definiert sind (einschließlich Leerzeichen U+20),
anderfalls \pyth{False}.

\item[\pyfunc{str.isspace()}] Gibt \pyth{True} zurück wenn der String
nur Leerraumzeichen (\emph{whitespace}) enthält, anderfalls \pyth{False}.

\item[\pyfunc{str.istitle()}] Gibt \pyth{True} zurück wenn der String
sog.\ \emph{titlecased strings} enthält, anderfalls \pyth{False}.
Ein \emph{titlecased string} besteht aus mindestens einem
Großbuchstaben, gefolgt von einem oder mehreren Kleinbuchstaben.

\item[\pyfunc{str.isupper()}]  Gibt \pyth{True} zurück wenn alle
\emph{cased characters} im String Großbuchstaben sind, anderfalls
\pyth{False}.
\end{description}
\fi

\subsection{Removing Spaces}
Using \pyth{str.lstrip} (left only), \pyth{str.rstrip} (right only),
or \pyth{str.strip} (both sides):

\begin{python}
>>> '   Tux   '.lstrip()
'Tux   '
>>> '   Tux   '.rstrip()
'   Tux'
>>> '   Tux   '.strip()
'Tux'
\end{python}

\section{Formatting Strings}
\label{sec.custom.string}

For historical reasons Python supports different ways of string formatting:

\begin{itemize}
\item Old-style with precent (like printf())
\item New-style with curly brackets and the method \pyth{str.format()}
\item F-strings (similar to new-style formatting)
\end{itemize}

Many examples are taken from \cite{petrigutman.pyformat}.

\subsection{Simple Formatting}
\begin{python}
>>> '%s %s' % ('one', 'two')
>>> '{} {}'.format('one', 'two')
'one two'
\end{python}

With number in curly brackets you refer to the argument position.
This is only possible with new-style formatting:

\begin{python}
>>> '{1} {0}'.format('one', 'two')
'two one'
\end{python}

\subsection{Converting Numbers into Strings}
\begin{itemize}
\item as a hexadecimal representation, see function \pyth{hex(NUMBER)}:
% (Typ \verb!x! und \verb!X!):
\begin{python}
>>> '0x%X' % 160
>>> '0x{:X}'.format(160)
'0xA0'
>>> '0x%x' % 160
>>> '0x{:x}'.format(160)
'0xa0'
\end{python}

\item as octale representation; see function \pyth{oct(NUMBER)}:
\begin{python}
>>> '0o%o' % 160
>>> '0o{:o}'.format(160)
'0o240'
\end{python}

\item as binary representation, only new style; see function \pyth{bin(NUMBER)}:
\begin{python}
>>> '0b{:b}'.format(160)
'0b10100000'
\end{python}

\item Integers:
\begin{python}
>>> '%d' % (42,)
>>> '{:d}'.format(42)
'42'
\end{python}

\item Floats:
\begin{python}
>>> '%f' % (3.141592653589793,)
>>> '{:f}'.format(3.141592653589793)
'3.141593'
\end{python}
\end{itemize}


\subsection{Aligning Strings}
\begin{itemize}
\item Align right:
\begin{python}
>>> '%10s' % ('test',)
>>> '{:>10}'.format('test')
'      test'
\end{python}

\item Align left:
\begin{python}
>>> '%-10s' % ('test',)
>>> '{:10}'.format('test')
'test      '
\end{python}

\item Center align (new-style only):
\begin{python}
>>> '{:^10}'.format('test')
'   test   '
\end{python}

\item Fill with a specific character (new-style only):
\begin{python}
>>> '{:_<10}'.format('test')
'test______'
\end{python}
\end{itemize}

\subsection{Cutting off Long Strings}
\begin{python}
>>> '%.5s' % ('xylophone',)
>>> '{:.5}'.format('xylophone')
'xylop'
\end{python}

\subsection{Using Named Placeholders}
\begin{python}
>>> data = {'first': 'Hodor', 'last': 'Hodor!'}
>>> '%(first)s %(last)s' % data
>>> '{first} {last}'.format(**data)
'Hodor Hodor!'
\end{python}

New-Style also allows access as name or index:

\begin{python}
>>> person = {'first': 'Jean-Luc',
              'last': 'Picard'}
>>> '{p[first]} {p[last]}'.format(p=person)
'Jean-Luc Picard'
>>> data = [1, 5, 10]
>>> '{d[0]}, {d[2]}'.format(d=data)
'1, 10'
\end{python}

\subsection{Applying Parameterizable Formats}
Old-Style is limited here, new-style offers the most possibilities:

\begin{python}
>>> '{:{align}{width}}'.format('test',
                               align='^',
                               width='10')
'   test   '
\end{python}


\begin{thebibliography}{10}
% Sort lastname author
\bibitem{petrigutman.pyformat}
\bauthor{Ulrich Petri \& Horst Gutmann},
\btitle{Using \% and \texttt{.format()} for great good!},
\url{https://pyformat.info}

\bibitem{python.string}
\bauthor{Python.org},
\btitle{string — Common string operations},
\url{https://docs.python.org/3.6/library/string.html#module-string}

\bibitem{python.literals}
\bauthor{Python.org},
\btitle{String and Bytes literals},
\url{https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals}
\end{thebibliography}



\end{multicols}
\end{document}
