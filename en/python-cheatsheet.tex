%
\documentclass[a4paper,landscape,10pt,USenglish]{scrartcl}
\usepackage[USenglish]{babel}
\usepackage{cheatsheet}

\usepackage[simplified]{pgf-umlcd}

%opening
\title{Python3 Cheatsheet}
\subtitle{Introduction and Overview to Python}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Introduction}
There are several Python programs, separated by version:

\begin{tabular}{lp{.5\textwidth}}
\verb!python!  & usually a link to \verb!python2! \\
\verb!python2! & calls Python 2 \\
\verb!python3! & calls Python 3
\end{tabular}

Executing of \verb!python! or \verb!pythonX! calls the interactive Python shell.

\subsection{Prompt}
Inside the interactive Python shell:

\begin{python}
>>> # the Python prompt
... # prompt when continuing lines
\end{python}


\subsection{Zen of Python}
Run inside the interactive Python shell:

\begin{python}
>>> import this
\end{python}

\subsection{Indentations}
Python code \emph{has} to be indented. Spaces (U+0020) or tabs (U+0009) are
allowed. It's recommend to indent your code with \emph{either} one.


\subsection{Help in Python}

\begin{python}
>>> # call help of a object:
>>> help(IOError)
>>> # call interactive help:
>>> help()
>>> # Press CTRL+D to exit help
>>> # Show all current variables:
>>> dir()
>>> # Only inside IPython:
>>> IOError?
\end{python}

\subsection{Header of a Python Script}

\begin{python}
#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
\end{python}


\section{Reserved Keywords}
See also \pyth{import keyword} or \pyth{keyword.kwlist}:

\begin{verbatim}
False        None         True         and
as           assert       break        class
continue     def          del          elif
else         except       finally      for
from         global       if           import
in           is           lambda       nonlocal
not          or           pass         raise
return       try          while        with
yield
\end{verbatim}


\section{Builtin Datatypes}
See also \url{https://docs.python.org/3/library/stdtypes.html}.
Display type with function \pyth{type(obj)}.

\begin{itemize}
\item \iqq{Nothing}: \pyth{None}
\item Boolean values: \pyth{True}/\pyth{False}, \pyth{bool(obj)}
\item Integers (\pyth{2}, \pyth{int(obj)}),
      floats (\pyth{11.7}, \pyth{float(obj)}),
      complex numbers (\pyth{1-5j}, \pyth{complex(obj)})
\item Sequences: lists, tuples, \pyth{range}
\item Binary sequences: \pyth{bytes}, \pyth{bytearray}, \pyth{memoryview}
\item Strings: \pyth{str}
\item Dictionaries/Hashes: \pyth{dict}
\item Sets: \pyth{set}, \pyth{frozenset}
\end{itemize}

\section{Assignments}

\begin{python}
>>> # Simple:
>>> a=1
>>> # Parallel, "unpacking":
>>> a, b = 1, 2
>>> # Swap two values:
>>> a, b = b, a
\end{python}


\section{Control Structures}
\subsection{The for Instruction}
The \pyth{for} instruction is a loop over iterable objects. It iterates
over every element in the iterable object and returns the current value:


\begin{python}
>>> for i in ["Hello", "World"]:
...    print(i)
Hello
World
\end{python}


\subsection{The while Instruction}
The \pyth{while} instruction is a loop. As long as the test is true,
the loop is executed:

\begin{python}
>>> i=10
>>> while i != 0:
...   print(i)
...   i -= 1
\end{python}

Python does not know an \pyth{until} loop!

\subsection{The if Instruction}
The \pyth{if} instruction tests expressions:

\begin{python}
>>> i=3
>>> if i<3:
...   print("Smaller than 3!")
... elif i==3:
...   print("Equal to 3!")
... else:
...   print("Greater than 3!")
Genau 3!
\end{python}

\section{Functions}
With the \pyth{def} instruction you create a new function. It is recommended
to use \emph{docstrings} to document the function and its purpose:

\begin{python}
>>> # Function without an argument:
>>> def f(): pass
>>> # Function with required argument:
>>> def f(x): pass
>>> # Function with optional argument:
>>> def f(x=0): pass
>>> # Function with required and optional argument:
>>> def f(x, y=0): pass
>>> # Function with tuple and dict:
>>> def f(*astuple, **asdict):
...   """Shows tuple and Dict"""
...   print(astuple, "---", asdict)
...   # It's allowed to return more than one value:
...   return 1, 5, 6
>>> f(1, 5, 7, one=1, two=2, three=3)
((1, 5, 7) --- {'three': 3, 'one': 1, 'two': 2})
(1,5)
\end{python}


\section{Sequences}
An iterable object which is capable of returning its elements one by one.
Furthermore, a sequence defines element-wise access via indices and supports
the function \pyth{len()}.

Examples of sequences are lists, tuples, strings, tuples, and bytes.

\begin{python}
>>> L = [1, 5, 23, 17, 11, 2000, 16, -5, 10]
>>> len(L)
9
>>> L[2]
23
>>> L[3:6]
[17, 11, 2000]
>>> L[3:-1:2]
[17, 2000, -5]
\end{python}


\section{Generators}
A generator is a function with \pyth{yield}. Returns the next value
at each loop pass or \pyth{next} until the generators is \gquote{exhausted}.
An exhausted generator raises a \pyth{StopIteration} (usually hidden when
using for loops):

\begin{python}
>>> def countdown(n):
...    print("Counting down!")
...    while n > 0:
...       yield n
...       n -= 1
>>> five = countdown(5)
>>> five
<generator object countdown at 0x7f0847767708>
>>> for i in five:
...    print(i, end=" ")
Counting down!
5 4 3 2 1
\end{python}

A generator can also be advanced manually using \pyth{next}:

\begin{python}
>>> two = countdown(2)
>>> next(two)
Counting down!
2
>>> next(two)
1
>>> next(two)
StopIteration:
\end{python}



\section{List Comprehensions}
Compact \pyth{for} loop as list expression.

\begin{python}
>>> # Create a list with numbers from [1..9]
>>> a = range(1, 10)
>>> # Square it:
>>> a2 = [x**2 for x in a]
>>> # Only all even numbers:
>>> a3 = [x for x in a2 if x % 2 == 0 ]; a3
[4, 16, 36, 64]
\end{python}


\section{Generator Expressions}
Expression that returns an iterator; saves memory space.

\begin{python}
>>> # sum of squares 0, 1, 4, ... 81
>>> sum(i*i for i in range(10))
285
>>> g = (i*i for i in range(10))
>>> g
<generator object <genexpr> at 0x7f08445b8360>
>>> sum(g)
285
\end{python}

\section{Exception Handling}
Catches exceptions:

\begin{python}
>>> try:
...    f = open("foo", "r")
... except IOError as error:
...    print(error)
... else:
...    # Optional, if no exception was raised
... finally:
...    # Optional, is always executed
[Errno 2] No such file or directory: 'foo'
>>> # Raise an exception manually:
>>> raise IOError("Nothing found!")
\end{python}

Exception hierarchy:

%\begin{center}
\begin{tikzpicture}%[show background grid]
\begin{class}[text width=3cm]{BaseException}{0,0}
\end{class}
\begin{class}[text width=3cm]{SystemExit}{1,1}
\inherit{BaseException}
\end{class}
\begin{class}[text width=4cm]{KeyboardInterrupt}{5,0}
\inherit{BaseException}
\end{class}
\begin{class}[text width=3cm]{Exception}{1,-1}
\inherit{BaseException}
\end{class}
\begin{class}[text width=3cm]{Other Exceptions}{1,-2}
\inherit{Exception}
\end{class}
\end{tikzpicture}
%\end{center}


\section{Files}

\begin{python}
>>> # Read:
>>> f = open("data.txt", "r")
>>> # Read next line:
>>> f.readline()
>>> # Iterate over all lines in the file:
>>> for i in open("data.txt", "r"):
...   print(i)
>>> # Write line in file:
>>> out = open("foo.txt", "w")
>>> out.write("output string")
>>> # Close file
>>> out.close()
\end{python}

Usually it's easier to work with a \pyth{with} instruction:

\begin{python}
>>> with open("data.txt", "r") as myfile:
...   for line in myfile:
...     print(i)
\end{python}

At the end of the \pyth{with} block ,the file is automatically closed.

\section{Standard Library}
Batteries included:

\begin{description}
\item[Algorithms]
    \lib{contextlib}, \lib{functools}, \lib{itertools}, \lib{operator}
\item[Application Building Blocks]
    \lib{argparse}, \lib{atexit}, \lib{cmd},
    \lib{configparser}, \lib{fileinput}, \lib{getopt}, \lib{getpass}, \lib{logging},
    \lib{readline}, \lib{sched}, \lib{shlex}
\item[Concurrency]
    \lib{asyncio}, \lib{concurrent.futures}, \lib{multiprocessing}, \lib{signal},
    \lib{subprocess}, \lib{threading}
\item[Cryptography] \lib{hashlib}, \lib{hmac}
\item[Data Compression and Archiving]
    \lib{bz2}, \lib{gzip}, \lib{tarfile}, \lib{zipfile}, \lib{zlib}
\item[Data Persistence and Exchange]
    \lib{csv}, \lib{dbm}, \lib{pickle}, \lib{shelve}, \lib{sqlite3},
    \lib{xml.etree.ElementTree}
\item[Data Structures]
    \lib{array}, \lib{bisect}, \lib{collections}, \lib{copy},
    \lib{enum}, \lib{heapq}, \lib{pprint}, \lib{queue}, \lib{struct}, \lib{weakref}
\item[Dates and Times] \lib{calendar}, \lib{datetime}, \lib{time}
\item[Email] \lib{imaplib}, \lib{mailbox}, \lib{smtpd}, \lib{smtplib}
\item[File System]
    \lib{codecs}, \lib{filecmp}, \lib{fnmatch}, \lib{glob}, \lib{io}, \lib{linecache},
    \lib{mmap}, \lib{os.path}, \lib{pathlib}, \lib{shutil}, \lib{tempfile}
\item[I18N and L10N]
    \lib{gettext}, \lib{locale}
\item[The Internet]
    \lib{base64}, \lib{http.cookies}, \lib{http.server},
    \lib{json}, \lib{urllib.parse}, \lib{urllib.request},
    \lib{urllib.robotparser}, \lib{uuid}, \lib{webbrowser}, \lib{xmlrpc.client}, \lib{xmlrpc.server}
\item[Language Tools]
    \lib{abc}, \lib{dis}, \lib{inspect}, \lib{warnings}
\item[Mathemathics]
    \lib{decimal}, \lib{fractions}, \lib{math}, \lib{random}, \lib{rational}
\item[Modules and Packages]
    \lib{importlib}, \lib{pkgutil}, \lib{zipimport}
\item[Networking]
    \lib{ipaddress}, \lib{select}, \lib{selectors}, \lib{socket}, \lib{socketserver}
\item[Runtime Features]
    \lib{gc}, \lib{os}, \lib{platform}, \lib{resource}, \lib{site}, \lib{sys}, \lib{sysconfig}
\item[Text] \lib{difflib}, \lib{re}, \lib{string}, \lib{textwrap}
\end{description}


\section{Modules}

\begin{python}
>>> # Load 'sys' modul:
>>> import sys
>>> # Only load function 'join' from os.path:
>>> from os.path import join
>>> # Load everything(!) from modul os.path:
>>> # (BE CAREFUL):
>>> from os.path import *
>>> # Search path for Python modules:
>>> sys.path
\end{python}


\section{Style Questions: LBYL vs.\ EAFP}
\begin{description}
\item[EAFP (engl.\ \emph{easier to ask for forgiveness than permission})]
assumes that the error case is the exception, so error are caught via
\pyth{try...except}:

\begin{python}
try:
    x = my_dict["key"]
except KeyError:
    # 'key' not found
\end{python}

\item[LBYL (engl.\ \emph{look before you leap})]
tests conditions before anything is executed or looked up:

\begin{python}
if "key" in my_dict:
    x = my_dict["key"]
else:
    # 'key' not found
\end{python}
\end{description}


\section{Links}

\begin{itemize}
 \item \url{http://docs.python.org}
 \item \url{http://diveinto.org/python3/}
 \item \url{http://hyperpolyglot.org/scripting}
 \item \url{http://www.doughellmann.com/PyMOTW/}
\end{itemize}

\end{multicols}
\end{document}
