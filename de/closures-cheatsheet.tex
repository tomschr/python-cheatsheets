\documentclass[a4paper,landscape,10pt,ngerman]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage{cheatsheet}

\usepackage[simplified]{pgf-umlcd}

%opening
\title{Closures}
% \titlehead{fooo}
% \subtitle{Erklärung, Beispiele und Best Practices}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}


%% -----------------------------------------------------------
\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}\label{sec.definition}
Eine \emph{Closure}\cite{pythonsimplified.closures,realpython.closures}
(Deutsch: \emph{verschlossene Funktion})
ist eine Funktion die auf Variablen aus ihrem umgebenden Kontext zugreifen kann, auch wenn dieser Kontext bereits beendet ist. 

%Verkürzt formuliert: Objekte sind Daten mit angehängten Methoden,
%Closures sind Funktionen mit angehängten Daten.


\section{Anwendungen}
\begin{description}
    \item[Decorator-Funktionen]
    Closures sind die Grundlage für Dekoratoren in Python
    (siehe Spickzettel\cite{tomschr.decorators}).
    \item[Fabrikfunktionen (engl.\ \gquote{factory functions})]
    Closures können verwendet werden, um Funktionen mit vorgegebenen Einstellungen zu erstellen. Dies ist nützlich, wenn Sie mehrere ähnliche Funktionen mit unterschiedlichen Parametern erstellen möchten.
    \item[Callback-Funktionen]
    Closures können als Callbacks verwendet werden, um Funktionen zu übergeben, die später aufgerufen werden sollen. 
    % Wird häufig in GUI-Programmierung und Webentwicklung verwendet.
    \item[Funktionale Programmierung] Closures werden oft in der funktionalen Programmierung verwendet, um Funktionen zu erstellen, die mit bestimmten Parametern vorkonfiguriert sind.
\end{description}


\section{Vorteile}
\begin{itemize}
    \item \formaltitle{Vermeiden von unnötigen Klassen}
    Sollte eine Klasse (abgesehen von \pyth{__init__()}) nur eine
    Methode besitzen, könnte eine Closure lesbarer und einfacher sein.

    \item \formaltitle{Vermeiden von globalen Variablen}
    Closures helfen, den Einsatz von globalen Variablen zu reduzieren,
    was Struktur, Lesbarkeit, Wartbarkeit und Testbarkeit des Codes verbessert.

    \item \formaltitle{Erhaltung von Zuständen}
    Closures ermöglichen es, Zustände zwischen Funktionsaufrufen zu
    speichern, was besonders bei kontinuierlich aktualisierten Berechnungen nützlich ist.

    \item \formaltitle{Unterstützt das Verstecken von Daten}
    Die innere Funktion einer Closure kann nur über die äußere Funktion aufgerufen werden, und ihre freien Variablen sind nur innerhalb der Closure sichtbar. Diese Eigenschaften fördern das Verstecken von Daten.

    \item \formaltitle{Unterstützt die Implementierung von Decorators}
    Closures bilden die Grundlage für Decorators\cite{tomschr.decorators} in Python, die Funktionen erweitern oder modifizieren, ohne den Funktionscode selbst zu ändern.
\end{itemize}

\section{Nachteile}
\begin{itemize}
    \item \formaltitle{Komplexität und Lesbarkeit}
    Closures können den Code komplexer und schwieriger zu verstehen machen, insbesondere für Entwickler, die mit diesem Konzept nicht vertraut sind. % Das Verfolgen von geschlossenen Variablen und deren Werten kann den Code schwerer nachvollziehbar machen.

    \item \formaltitle{Speicherverbrauch}
    Closures halten Variablen aus ihrem Kontext im Speicher, was zu höherem Speicherverbrauch führen kann, besonders bei längerer Lebensdauer oder häufiger Nutzung.

    \item \formaltitle{Leistung}
    Closures können die Leistung beeinträchtigen, da sie zusätzlichen Speicher für Variablen aus dem äußeren Kontext benötigen.
\end{itemize}

\section{Die LEGB-Regel}\label{sec.legb-rule}
Nicht direkt ein Bestandteil von Closures, aber wichtig für das Verständnis von Closures ist die \emph{LEGB-Regel}\cite{pythonsimplified.legb-rule}.

Python bestimmt die Reihenfolge der Namensauflösung über verschiedene
\emph{Geltungsbereiche} (engl.\ \emph{scopes}) nach der \emph{LEGB-Regel}:

\begin{description}
    \item[Lokal (L)] Der innerste Bereich. Umfasst lokale Variablen innerhalb der aktuellen Funktion oder Methode.
    \item[Enclosing (E)] Der umgebende/verschachtelte Bereich.
    Der Geltungsbereich aller einschließenden Funktionen (Funktionen, die von der aktuellen Funktion aus aufgerufen werden).
    \item[Global (G)] Der globale Bereich. Umfasst globale Variablen, die auf Modul- oder Skriptebene definiert sind.
    \item[Built-in (B)] Der eingebaute Bereich. Umfasst eingebaute Python-Objekte, -Funktionen und -Ausnahmen (z.~B. \pyth{len}, \pyth{print} usw.) umfasst.
\end{description}

Beim Suchen eines Namens in Python wird der Geltungsbereich in der Reihenfolge L-E-G-B durchsucht.


\section{Merkmale von Closures}\label{sec.features}
\begin{description}
    \item[Innere Funktion] Eine Funktion, die
    innerhalb einer anderen Funktion definiert ist.
    % Daher findet man auch häufig den Begriff der \emph{verschachtelten oder eingeschlossene Funktion}.
    Die innere Funktion wird von der äußeren Funktion zurückgegeben. Des
    Weiteren kann die innere Funktion auf Variablen der äußeren Funktion zugreifen.

    \item[\gquote{Freie} Variablen]
    Eine Variable, die im inneren Funktionsumfang verwendet wird, aber nicht dort definiert wurde. Stattdessen wird sie aus einem äußeren, umschließenden Scope \gquote{geerbt}.
    Hierbei greift der \emph{Enclosed}-Teil der LEGB-Regel
    (siehe Abschnitt~\ref{sec.legb-rule}).
    
    \item[Lebensdauer der Variablen] Die \gquote{freien Variablen} 
    der äußeren Funktion bleiben im Speicher erhalten, solange die innere 
    Funktion verwendet wird auch wenn die äußere Funktion bereits beendet ist.
\end{description}


\section{Struktur einer Closure ohne Argumente}
Eine Closure die alle Merkmalen von Abschnitt~\ref{sec.features} enthält, jedoch
keine weiteren Argumente (sowohl für die äußere als auch für die innere Funktion) benötigt, sieht wie folgt aus:

\begin{python}
# (1) The outer function definition without arguments:
def outer_function():
    name = 'Tux' # a local variable

    # (2) The inner function definition:
    def closure():
        # (3) Access of the free variable name
        # and return of the result:
        return f"Hello {name}"

    # (4) Return the inner function object:
    return closure
\end{python}

Wird \pyth{outer_function} aufgerufen, wird die innere Funktion \pyth{closure} zurückgegeben:

\begin{python}
myclosure = outer_function()
\end{python}

Da \pyth{outer_function} bereits beendet ist, bleibt die freie Variable
\pyth{name} erhalten. Die Closure \pyth{myclosure} kann nun aufgerufen werden und gibt den Wert von \pyth{name} als Gruß aus:

\begin{python}
myclosure() # Ausgabe: Hello Tux
\end{python}


\section{Das \pyth{nonlocal}-Schlüsselwort}
Soll der Wert einer freie Variable geändert werden benötigst du das \pyth{nonlocal}-Schlüsselwort:

\begin{python}
def outer_function():
    name = 'Tux' # local variable

    def closure():
        nonlocal name
        name = name.upper()
        result = f"Hello {name}"
        return result

    return closure
\end{python}


\section{Struktur einer Closure mit Argumenten}\label{sec.closure-arguments}
Je nach Bedarf, kann eine Closure auch Argumente entgegennehmen, sowohl für die äußere als auch für die innere Funktion.

Eine Closure die alle Merkmalen von Abschnitt~\ref{sec.features} enthält und diese zusätzlichen Argumente hat, sieht wie folgt aus:

\begin{python}
# (1) The outer function definition with an argument:
def outer_function(greeting="Hello"):

    # (2) The inner function definition with an argument:
    def closure(name="Tux"):
        # (3) Access of the free variables greeting and name
        # and return of the result:
        return f"{greeting} {name}"

    # (4) Return the inner function object:
    return closure
\end{python}

Wird \pyth{outer_function} aufgerufen, wird der Standardgruß \pyth{Hello} verwendet und die innere Funktion \pyth{closure} zurückgegeben:

\begin{python}
myclosure = outer_function()
\end{python}

Die Funktion \pyth{myclosure} enthält die freie Variable \pyth{greeting}.

Da \pyth{outer_function} bereits beendet ist, bleibt die freie Variable
\pyth{greeting} erhalten. Die Closure \pyth{myclosure} kann nun aufgerufen werden, entweder mit einem Standardnamen (\pyth{Tux}) oder einem anderen Namen.
Diese enthält wiederrum eine freie Variable \pyth{name}:

\begin{python}
myclosure() # => 'Hello Tux'
myclosure("Hi") # => 'Hi Tux'
\end{python}

\section{Beispiele}
\subsection{Multiplikator-Closure}
Eine Closure, die eine Funktion erstelle, die jede Zahl mit einem
festgelegtem Faktor multipliziert:

\begin{python}
def make_multiplier(factor):
    """
    Returns a function that multiplies a number by a factor.
    """
    def multiply(number):
        return number * factor
    return multiply
\end{python}

Wird wie folgt verwendet:

\begin{python}
double = make_multiplier(2)
triple = make_multiplier(3)
print(double(5)) # => 10
print(triple(5)) # => 15
\end{python}

\subsection{Zähler-Closure}\label{sec.counter-closure}
Eine Closure, die eine Funktion erstellt, die bei jedem Aufruf einen
Zähler erhöht.

\begin{python}
def make_counter():
    """
    Returns a function that increments a counter by 1
    each time it is called.
    """
    count = 0
    def counter():
        nonlocal count
        count += 1
        return count
    return counter
\end{python}

Wird wie folgt verwendet:

\begin{python}
counter = make_counter()
print(counter()) # => 1
print(counter()) # => 2
print(counter()) # => 3
\end{python}

\subsection{HTML-Closure}
Eine Closure, die eine Funktion erstellt, die Text in HTML
formatiert:

\begin{python}
def make_html(tag):
    """
    Returns a function that wraps text in an HTML tag.
    """
    def html(text):
        return f"<{tag}>{text}</{tag}>"
    return html
\end{python}

Wird wie folgt verwendet:

\begin{python}
bold = make_html('b')
italic = make_html('i')
print(bold('Hello')) # => <b>Hello</b>
print(italic('World')) # => <i>World</i>
\end{python}


\section{Attribute in Closures}
Closure können auch weitere Daten oder Funktionen innerhalb des Closures-Objekts speichern. Dies kann nützlich sein, um zusätzliche Informationen zu speichern oder um die Closure zu erweitern.

Der folgende Code combiniert die Zähler-Closure aus
Abschnitt~\ref{sec.counter-closure} mit zusätzlichen Attributen:

\begin{python}
def make_greeter(greeting):
    """
    Returns a function that greets a person by name.
    """
    counter = 0
    def greeter(name):
        nonlocal counter
        counter += 1
        return f"{greeting}, {name}!"

    def update_greeting(new_greeting):
        nonlocal greeting
        greeter.greeting = new_greeting
        greeting = new_greeting
    
    def get_counter():
        # This is NOT needed as it's only
        # read-only access:
        # nonlocal counter
        return counter

    # Add additional attributes to its closure object:
    greeter.greeting = greeting
    greeter.update_greeting = update_greeting
    greeter.get_counter = get_counter
    return greeter
\end{python}

Wird wie folgt verwendet:

\begin{python}
greeter = make_greeter('Hello')
print(greeter('Tux')) # => Hello, Tux!
print(greeter('Penguin')) # => Hello, Penguin!
print(greeter.get_counter()) # => 2
greeter.update_greeting('Hi')
print(greeter('Tux')) # => Hi, Tux!
\end{python}

\section{Testen auf Closures}
Um zu überprüfen, ob eine Funktion eine Closure ist, kann dies auf zwei Arten erfolgen:

\begin{itemize}
    \item Über die \pyth{__closure__}-Eigenschaft
    \item Über das \pyth{inspect}-Modul
\end{itemize}

\subsection{Über die \pyth{__closure__}-Eigenschaft}
Die \pyth{__closure__}-Eigenschaft einer Funktion gibt eine Tupel von Zellen zurück, die die freien Variablen der Closure enthalten. Wenn die Funktion keine Closure ist, ist \pyth{__closure__} \pyth{None}.

Dies kann über diese Funktion überprüft werden:

\begin{python}
def is_closure(func):
    """
    Check if a function is a closure.
    """
    return (
        func.__closure__ is not None and
        len(func.__closure__) > 0
    )
\end{python}


\subsection{Über das \pyth{inspect}-Modul}
Mit Hilfe der Funktion \pyth{inspect.isfunction()} und \pyth{inspect.getclosurevars()} aus dem \pyth{inspect}-Modul\cite{python.inspect} kann ebenfalls überprüft werden, ob eine Funktion eine Closure ist.

Die Funktion \pyth{inspect.getclosurevars()} gibt ein \pyth{ClosureVars}-Objekt zurück, das Informationen über die freien Variablen der Closure enthält
(Attribut \pyth{nonlocals}).
Wenn die Funktion keine Closure ist, sind die freien Variablen leer.

\begin{python}
import inspect

def is_closure(func):
    """
    Check if a function is a closure.
    """
    closure_vars = inspect.getclosurevars(func)
    return bool(closure_vars.nonlocals)
\end{python}


\begin{thebibliography}{10}
\bibitem{pythonsimplified.closures}
    \bauthor{Chetan Ambi}
    \btitle{Beginners Guide to Python Closures (2021)},
    \url{https://pythonsimplified.com/beginners-guide-to-python-closures/}

\bibitem{pythonsimplified.legb-rule}
    \bauthor{Chetan Ambi},
    \btitle{Understanding namespaces and scope in Python (2021)},
    \url{https://pythonsimplified.com/understanding-namespaces-and-scope-in-python/}

\bibitem{realpython.closures}
   \bauthor{Leodanis Pozo Ramos},
   \btitle{Python Closures: Common Use Cases and Examples (2024)},
   \url{https://realpython.com/python-closures/}

\bibitem{tomschr.decorators}
   \bauthor{Thomas Schraitle},
   \btitle{Decorators},
   \url{https://gitlab.com/tomschr/python-cheatsheets/-/releases}

\bibitem{python.inspect}
    \bauthor{Python Software Foundation},
    \btitle{inspect --- Inspect live objects},
    \url{https://docs.python.org/3/library/inspect.html}
\end{thebibliography}

\end{multicols}
\end{document}