\documentclass[a4paper,landscape,10pt,ngerman]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage{cheatsheet}

%opening
\title{Python3 Datentypen Spickzettel}
% \titlehead{fooo}
\subtitle{Strings}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}

\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}
\label{sec.dict.definition}
Strings (Zeichenketten) sind eine unveränderliche Sequenz von
Unicode-Codepunkten.

\section{Erzeugen von Strings}
Strings werden über \pyth{str} erzeugt oder direkt über
Anführungszeichen:

\begin{itemize}
\item in einfachen Anführungszeichen: \pyth{'hello'}
\item in doppelten Anführungszeichen: \pyth{"hello"}
\item in dreifachen Anführungszeichen: \pyth{"""hello"""} oder
\pyth{'''hello'''}
\end{itemize}

Lange oder unhandliche Strings können auf verschiedene Zeilen
aufgeteilt werden:

\begin{itemize}
\item Über Backslash:

\begin{python}
hello = "hello " \
        "world"
\end{python}

\item Über eine Klammer ohne(!) Backslash:
\begin{python}
hello = ("hello "
         "world")
\end{python}
\end{itemize}

Beides erzeugt \pyth{Hello World}.


\section{Erzeugen von Strings mit Prefix}
Über einen Präfix vor den Anführungszeichen lassen sich bestimmte
Eigenschaften steuern. Es sind definiert:

\begin{description}
\item[\texttt{b} Bytes Literals]
  erzeugen eine unverändliche Sequenz einzelner Bytes, d.~h.\ Ganzzahlen im
  Bereich von 0...256. Der Inhalt kann auch über ASCII-Zeichen dargestellt
  werden. Zeichen über den nummerischen Wert von 128 müssen über eine
  entsprechende Escape-Sequenz maskiert werden.

\item[\texttt{u} Unicode-String] dies ist der Standardwert und muss
  nicht explizit angegeben werden. Nur aus Kompatibilitätsgründen vorhanden.

\item[\texttt{r} Raw-String] Backslashes werden als normales
  Zeichen interpretiert.

\item[\texttt{f} f-String (Python \textgreater3.6)]
  ist ein sog.\ \emph{formatted string literal}. Der Inhalt enthählt einen
  Platzhalter in geschweiften Klammern der zur Laufzeit evaluiert wird:

\begin{python}
>>> name = "Tux"
>>> f"My name is {name}."
'My name is Tux.'
\end{python}
\end{description}

\section{Escape-Sequenzen}
Sofern kein \texttt{r}- oder \texttt{R}-Präfix anwesend ist, werden
Escape-Sequenzen in Strings- und Byte-Literals ähnlich wie in Standard C
interpretiert. Die folgende Aufstellung gibt einen Überblick. Die
komplette Liste gibt es unter \cite{python.literals}.

\begin{description}
\item[\texttt{\char`\\t}] ASCII Horizontal Tab (TAB)
\item[\texttt{\char`\\n}] ASCII Linefeed (LF)
\item[\texttt{\char`\\"}] doppelte Anführungszeichen
\item[\texttt{\char`\\\textquotesingle}] einfaches Anführungszeichen
\item[\texttt{\char`\\u\{xxxx\}}] Unicode-Zeichen
als 16-Bitwert (hexadezimal)
\item[\texttt{\char`\\U\{xxxxxxxx\}}] Unicode-Zeichen
als 32-Bitwert (hexadezimal)
\item[\texttt{\char`\\N\{<NAME>\}}]
% \texttt{\textbackslash N$\lbrace$NAME$\rbrace$}]
Benanntes Zeichen aus der Unicode-Datenbank (Groß- und Kleinschreibung
ist irrelevant)
\end{description}

\begin{python}
>>> 'Hello \'Tux\'!'
"Hello 'Tux'!"
>>> s1 = "\\textbf{Hello TeX}"
>>> s2 = r"\textbf{Hello TeX}"
>>> s1 == s2
True
>>> "Euro: \N{euro sign} oder \u20ac"
\end{python}
% 'Euro: € oder €'

\section{String-Methoden}

\subsection{Suchen und Ersetzen}
\pyfunc{str.find(sub[, start[, end]])}\par
\pyfunc{str.index(sub[, start[, end]])}\par
\pyfunc{str.replace(old, new[, count])}\par
\pyfunc{str.rfind(sub[, start[, end]])}\par
\pyfunc{str.rindex(sub[, start[, end]])}

\begin{python}
>>> "Hi Python!".find("Py") # wie .index("Py")
3
>>> "Hi Python!".find("py")
-1
>>> "Hi Python!".index("py")
ValueError: substring not found
>>> "Hi Python!".replace("Hi", "Hello")
'Hello Python!'
>>> "Hi! Python!".rfind("!")
10
\end{python}

\subsection{Strings auftrennen}

\pyfunc{str.splitlines([keepends])}\par
\pyfunc{str.split(sep=None, maxsplit=-1)}\par
\pyfunc{str.rsplit(sep=None, maxsplit=-1)}\par
\pyfunc{str.rpartition(sep)}

\begin{python}
>>> '1,2,3'.split(',')
['1', '2', '3']
>>> '1,2,3'.split(',', maxsplit=1)
['1', '2,3']
>>> '1,2,,3,'.split(',')
['1', '2', '', '3', '']
>>> "Hello\nWorld\n".splitlines()
['Hello', 'World']
>>> "Hello\nWorld\n".splitlines(True)
['Hello\n', 'World\n']
>>> "Hello\nWorld\n".partition('\n')
('Hello', '\n', 'World\n')
>>> "Hello\nWorld\n".rpartition('\n')
('Hello\nWorld', '\n', '')
\end{python}

\subsection{Strings kombinieren}

\begin{python}
>>> " ".join(["Hello", "World"])
'Hello World'
>>> "Hello" + " " + "World"
'Hello World'
\end{python}

\subsection{Strings ausrichten oder auffüllen}

\pyfunc{str.center(width[, fillchar])}\par
\pyfunc{str.ljust(width[, fillchar])}\par
\pyfunc{str.expandtabs(tabsize=8)}\par
\pyfunc{str.rjust(width[, fillchar])}\par
\pyfunc{str.zfill(width)}

\begin{python}
>>> "Tux".center(5)
' Tux '
>>> "Tux".center(5, '-')
'-Tux-'
>>> '1\t2'.expandtabs()
'1       2'
>>> '1\t2'.expandtabs(4)
'1   2'
\end{python}

\iffalse
\subsection{Strings umwandeln}

\pyfunc{str.capitalize()}\par
\pyfunc{str.casefold()}\par
\pyfunc{str.lower()}\par
\pyfunc{str.maketrans(x[, y[, z]])}\par
\pyfunc{str.swapcase()}\par
\pyfunc{str.translate(table)}\par
\pyfunc{str.title()}\par
\pyfunc{str.upper()}
\fi

\iffalse
\begin{python}
>>> "Hello Fuß".capitalize()
'Hello fuß'
>>> "Hello Fuß".casefold()
'hello fuss'
>>> "Hello Fuß".lower()
'hello fuß'
>>> "Hello Fuß".swapcase()
'hELLO fUSS'
>>> "Hello fuß".title()
'Hello Fuß'
>>> "Hello Fuß".upper()
'HELLO FUSS'
\end{python}
\fi


\subsection{Strings formatieren}

\pyfunc{str.format(*args, **kwargs)}\par
\pyfunc{str.format\_map(mapping)}
% printf-Style

\iffalse
\begin{python}
>>> "{} {}".format("Tux", "Penguin")
'Vorname: Tux, Nachname: Penguin'
>>> "{vname} {nname}".format(nname="Penguin",
                             vname="Tux")
'Vorname: Tux, Nachname: Penguin'
\end{python}
\fi

Für weitere Details, siehe Abschnitt~\ref{sec.custom.string} und
\url{https://docs.python.org/3.6/library/string.html#formatstrings}.


\subsection{Strings kodieren und dekodieren}
Python3 unterscheidet zwischen Strings (in Unicode) und Byte-Strings.
Um das eine in das andere umzuwandeln muss die Zeichensatz-Kodierung
angegeben werden:

\iffalse
% Die Standardkodierung ist über \pyth{sys.getfilesystemencoding()} abrufbar (meist UTF-8).
Der Unterschied wird deutlich bei einem Zeichen, das einen Unicode-Codepunkt
von größer 256 hat.
Das Euro-Zeichen (€ = U+20AC) wird als Unicode-String
als ein Zeichen gesehen, während es als Byte-String eine Sequenz von
drei Zeichen hat (kodiert als UTF-8):

\begin{python}
>>> s1 = '\N{euro sign}' # U+20AC
>>> b1 = bytes(s1, 'UTF-8')
b'12 \xe2\x82\xac'
>>> len(s), len(b)
(1, 3)
# Oder:
>>> b2 = s1.encode('utf-8')
>>> b1 == b2
True
\end{python}
\fi


\begin{description}
\item[von Unicode-String nach Byte-String]
  über die Funktion \pyth{byte(uni_string, encoding)} oder die str-Methode \pyth{.encode()}:

\begin{python}
>>> s1 = '\N{euro sign}' # U+20AC
>>> bytes(s1, 'utf-8') == s1.encode('utf-8')
True
\end{python}

\item[von Byte-String nach Unicode-String]
  über die Funktion \pyth{str(byte_string, encoding)} oder die bytes-Methode \pyth{.decode()}:

\begin{python}
# Euro als UTF-8 Stream
>>> b1 = b'\xe2\x82\xac'
>>> str(b1, 'utf-8') == b1.decode('utf-8')
True
\end{python}
\end{description}

Sollten bei der Umwandlung Fehler auftreten, werden die Ausnahmen
\pyth{UnicodeDecodeError} oder \pyth{UnicodeEncodeError} ausgelöst.

% str.encode(encoding="utf-8", errors="strict")


\subsection{Start und Ende eines Strings überprüfen}

\pyfunc{str.endswith(suffix[, start[, end]])}\par
\pyfunc{str.startswith(prefix[, start[, end]])}

\begin{python}
>>> "Python is great".startswith("Python")
True
>>> "Python is great".startswith(("Py", "XML"))
True
>>> "Python is great".startswith("Python", 6)
False
\end{python}

\subsection{String-Eigenschaften überprüfen}
Die Methoden beginnen alle mit \pyth{str.isNAME()} und liefern
\pyth{True} zurück wenn die Bedingung erfüllt ist, anderfalls
\pyth{False}:\linebreak
\pyfunc{str.isalnum()}, \pyfunc{str.isalpha()}, \pyfunc{str.isdecimal()},
\pyfunc{str.isdigit()}, \pyfunc{str.isidentifier()},
\pyfunc{str.islower()}, \pyfunc{str.isnumeric()},
\pyfunc{str.isprintable()}, \pyfunc{str.isspace()},
\pyfunc{str.istitle()}, \pyfunc{str.isupper()}.

\iffalse
\begin{description}
\item[\pyfunc{str.isalnum()}] Gibt \pyth{True} zurück wenn alle Zeichen
 alphanumerisch sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isalpha()}] Gibt \pyth{True} zurück wenn alle Zeichen
alphabetisch sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isdecimal()}] Gibt \pyth{True} zurück wenn alle Zeichen
Dezimalzeichen sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isdigit()}] Gibt \pyth{True} zurück wenn alle Zeichen
Zahlen sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isidentifier()}] Gibt \pyth{True} zurück wenn der
String ein gültiger Bezeichner ist, siehe \url{https://docs.python.org/3/eference/lexical_analysis.html#identifiers}.

\item[\pyfunc{str.islower()}] Gibt \pyth{True} zurück wenn alle
\emph{cased characters} im String Kleinbuchstaben sind, anderfalls
\pyth{False}.

\item[\pyfunc{str.isnumeric()}] Gibt \pyth{True} zurück wenn alle
Zeichen im String nummerisch sind, anderfalls \pyth{False}.

\item[\pyfunc{str.isprintable()}] Gibt \pyth{True} zurück wenn der String
nur Zeichen enthält, die im Unicode-Standard als \emph{Other} oder
\emph{Separator} definiert sind (einschließlich Leerzeichen U+20),
anderfalls \pyth{False}.

\item[\pyfunc{str.isspace()}] Gibt \pyth{True} zurück wenn der String
nur Leerraumzeichen (\emph{whitespace}) enthält, anderfalls \pyth{False}.

\item[\pyfunc{str.istitle()}] Gibt \pyth{True} zurück wenn der String
sog.\ \emph{titlecased strings} enthält, anderfalls \pyth{False}.
Ein \emph{titlecased string} besteht aus mindestens einem
Großbuchstaben, gefolgt von einem oder mehreren Kleinbuchstaben.

\item[\pyfunc{str.isupper()}]  Gibt \pyth{True} zurück wenn alle
\emph{cased characters} im String Großbuchstaben sind, anderfalls
\pyth{False}.
\end{description}
\fi

\subsection{Leerraum entfernen}
Über \pyth{str.lstrip} (nur links), \pyth{str.rstrip} (nur rechts)
oder \pyth{str.strip} (beidseitig):

\begin{python}
>>> '   Tux   '.lstrip()
'Tux   '
>>> '   Tux   '.rstrip()
'   Tux'
>>> '   Tux   '.strip()
'Tux'
\end{python}

\section{String-Formatierung}
\label{sec.custom.string}

Aus historischen Gründen unterstützt Python zwei Arten der String-Formatierung:

\begin{itemize}
\item Old-Style mit Prozent (wie bei printf())
\item New-Style mit geschweiften Klammern und der str-Methode \pyth{.format()}
\end{itemize}

Viele Beispiele sind aus \cite{petrigutman.pyformat} entnommen.

\subsection{Einfache Formatierung}
\begin{python}
>>> '%s %s' % ('one', 'two')
>>> '{} {}'.format('one', 'two')
'one two'
\end{python}

\subsection{Ganzzahl nach String}
\begin{itemize}
\item als hexadezimale Darstellung; vgl.~Funktion \pyth{hex(Zahl)}:
% (Typ \verb!x! und \verb!X!):
\begin{python}
>>> '0x%X' % 160
>>> '0x{:X}'.format(160)
'0xA0'
>>> '0x%x' % 160
>>> '0x{:x}'.format(160)
'0xa0'
\end{python}

\item als oktale Darstellung; vgl.~Funktion \pyth{oct(Zahl)}:
\begin{python}
>>> '0o%o' % 160
>>> '0o{:o}'.format(160)
'0o240'
\end{python}

\item als binäre Darstellung, nur New-Style; vgl.~Funktion \pyth{bin(Zahl)}:
\begin{python}
>>> '0b{:b}'.format(160)
'0b10100000'
\end{python}
\end{itemize}

\subsection{Positionierung}
Dies ist nur mit der New-Style-Formatierung möglich:
\begin{python}
>>> '{1} {0}'.format('one', 'two')
'two one'
\end{python}

\subsection{Ausrichten von Strings}
\begin{itemize}
\item Rechts ausrichten:
\begin{python}
>>> '%10s' % ('test',)
>>> '{:>10}'.format('test')
'      test'
\end{python}

\item Links ausrichten:
\begin{python}
>>> '%-10s' % ('test',)
>>> '{:10}'.format('test')
'test      '
\end{python}

\item Zentriert ausrichten (nur New-Style):
\begin{python}
>>> '{:^10}'.format('test')
'   test   '
\end{python}

\item Auffüllen mit einem bestimmten Zeichen (nur New-Style):
\begin{python}
>>> '{:_<10}'.format('test')
'test______'
\end{python}
\end{itemize}

\subsection{Lange Strings abschneiden}
\begin{python}
>>> '%.5s' % ('xylophone',)
>>> '{:.5}'.format('xylophone')
'xylop'
\end{python}

\subsection{Zahlen formatieren}
\begin{itemize}
\item Ganzzahlen:
\begin{python}
>>> '%d' % (42,)
>>> '{:d}'.format(42)
'42'
\end{python}

\item Fließkommazahlen:
\begin{python}
>>> '%f' % (3.141592653589793,)
>>> '{:f}'.format(3.141592653589793)
'3.141593'
\end{python}

\item Zahl rechts ausrichten:
\begin{python}
>>> '%4d' % (42,)
>>> '{:4d}'.format(42)
'  42'
\end{python}
\end{itemize}

\subsection{Benannte Platzhalter}
\begin{python}
>>> data = {'first': 'Hodor', 'last': 'Hodor!'}
>>> '%(first)s %(last)s' % data
>>> '{first} {last}'.format(**data)
'Hodor Hodor!'
\end{python}

New-Style erlaubt auch den Zugriff als Name oder Index:
\begin{python}
>>> person = {'first': 'Jean-Luc',
              'last': 'Picard'}
>>> '{p[first]} {p[last]}'.format(p=person)
'Jean-Luc Picard'
>>> data = [1, 5, 10]
>>> '{d[0]}, {d[2]}'.format(d=data)
'1, 10'
\end{python}

\subsection{Parametrisierbare Formate}
Old-Style ist hier limitiert, die meisten Möglichkeiten bietet
New-Style:

\begin{python}
>>> '{:{align}{width}}'.format('test',
                               align='^',
                               width='10')
'   test   '
\end{python}


\begin{thebibliography}{10}
% Sort lastname author
\bibitem{petrigutman.pyformat}
\bauthor{Ulrich Petri \& Horst Gutmann},
\btitle{Using \% and \texttt{.format()} for great good!},
\url{https://pyformat.info}

\bibitem{python.string}
\bauthor{Python.org},
\btitle{string — Common string operations},
\url{https://docs.python.org/3.6/library/string.html#module-string}

\bibitem{python.literals}
\bauthor{Python.org},
\btitle{String and Bytes literals},
\url{https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals}
\end{thebibliography}



\end{multicols}
\end{document}
