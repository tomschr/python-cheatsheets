\documentclass[a4paper,landscape,10pt,ngerman]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage{cheatsheet}

\usepackage[simplified]{pgf-umlcd}

%opening
\title{Decorators}
% \titlehead{fooo}
\subtitle{Funktionen mit wiederverwendbarer und konfigurierbarer Logik umhüllen}
\author{Thomas Schraitle}
\date{\today}
\pagestyle{empty}


%% -----------------------------------------------------------
\begin{document}
\RaggedRight
\footnotesize
\maketitle

\begin{multicols}{3}
\setlength{\columnseprule}{1pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\section{Definition}
Ein \emph{Decorator} ist ein Entwurfsmuster,
das die Funktionalität einer Funktion oder Klasse ändert,
indem sie in eine andere Funktion oder Klasse \gquote{eingepackt} wird
ohne die Definition der dekorierten Funktion oder Klasse zu ändern.

\iffalse
Ein \emph{Decorator} ist eine Python-Funktion oder -Klasse, die eine andere Funktion oder Methode umhüllt, um zusätzliche Funktionalität hinzuzufügen oder ihr Verhalten zu ändern, ohne den eigentlichen Code der Funktion zu ändern.
\fi

\section{Anwendungen}
\begin{description}
  \item[Logging] Protokollierung von Funktionsaufrufen.
  \item[Timing] Messung der Ausführungszeit einer Funktion, siehe Abschnitt~\ref{sec.exa.timing}.
  \item[Caching] Speichern von Funktionsein- oder ausgaben, um die Ausführung zu beschleunigen.
  \item[Validierung] Überprüfen von Funktionsargumenten.\cite{beartype,pydantic,typeguard}
  \item[Authentifizierung] Überprüfen von Benutzerberechtigungen.
  \item[Wiederholen] Wiederholen eines Funktionsaufrufs bei einem Fehler.
  \item[Transformieren] Ändern des Rückgabewerts einer Funktion.
  \item[Vieles mehr\ldots]
\end{description}

\section{Technische Bestandteile}
Ein Decorator besteht typischerweise aus folgenden Komponenten:

\begin{itemize}
  \item \formaltitle{Die ursprüngliche Funktion/Klasse.} Das Objekt, das an den
  Decorator übergeben wird.
  \item \formaltitle{Die Decorator-Funktion.} Die (äußere) Funktion, die den
  Decorator selbst definiert. Sie erwartet eine Funktion oder Klasse als
  Argument, evlt.\ weitere Argumente und gibt eine geänderte Version
  der dekorierten Funktion/Klasse zurück.
  \item \formaltitle{Die Wrapper-Funktion.} Eine innere Funktion (meist \pyth{wrapper} genannt) innerhalb des Decorators, die die ursprüngliche Funktion umhüllt. Diese Funktion kann die Eingabe oder Ausgabe der ursprünglichen Funktion ändern.
  \item \formaltitle{functools.wraps} (optional, jedoch empfohlen):
  Ein Decorator der die Metadaten der ursprünglichen Funktion beibehält.
\end{itemize}

\section{Vorteile}
\begin{itemize}
  \item \formaltitle{Erweiterbarkeit} Decorators ermöglichen es, Funktionen zu erweitern, ohne den ursprünglichen Code zu ändern.

  \item \formaltitle{Trennung der Verantwortlichkeiten} Decorators erlauben die saubere Trennung von Kernlogik und Zusatzlogik. Sie helfen, die Zusatzlogik (z.~B.\ Logging, Authentifizierung) von der ursprünglichen Funktion zu trennen.

  \item \formaltitle{Wiederverwendbarkeit} Decorators können auf mehrere Funktionen angewendet werden.

  \item \formaltitle{Modularität} Decorators ermöglichen es, die Funktionalität einer Funktion in separaten, wiederverwendbaren Einheiten zu organisieren.

  \item \formaltitle{DRY-Prinzip (\gquote{Don't repeat yourself}).} Decorators helfen, Redundanzen zu vermeiden indem sie wiederverwendbaren Code bündeln.
\end{itemize}


\section{Nachteile}
\begin{itemize}
  \item \formaltitle{Komplexität} Decorators können den Code komplexer machen, insbesondere wenn sie verschachtelt sind.
  \item \formaltitle{Versteckte Logik} Decorators können die Lesbarkeit des Codes beeinträchtigen, wenn die zusätzliche Logik nicht offensichtlich ist.
  \item \formaltitle{Debugging} Decorators können das Debuggen erschweren, da sie die Ausführung von Code verbergen.
  \item \formaltitle{Performance} Decorators können die Leistung beeinträchtigen, insbesondere wenn sie unnötig verschachtelt sind.
  \item \formaltitle{Vererbung} Decorators können das Verhalten von vererbten Methoden oder Klassen beeinflussen.
\end{itemize}

\section{\allowhyph{Decorators in der Standard\-bibliothek}}
Python bietet eine Reihe von Decorators in der Standardbibliothek an, die häufig in der Praxis verwendet werden:

\begin{itemize}
  \item \textbf{\texttt{@staticmethod}} Deklariert eine statische Methode.
  \item \textbf{\texttt{@classmethod}} Deklariert eine Klassenmethode.
  \item \textbf{\texttt{@property}} Deklariert eine Eigenschaft.
  \item \textbf{\pyth{@abc.abstractmethod}} Deklariert eine abstrakte Methode.
  \item \textbf{\pyth{@atexit.register}} Registriert eine Funktion, die beim Beenden des Programms aufgerufen wird.
  \item \textbf{\pyth{@dataclasses.dataclass}} Deklariert eine Datenklasse.
  \item \textbf{\pyth{@enum.unique}} Deklariert ein eindeutiges Enum-Element.
  \item \textbf{\pyth{@enum.verify}} Überprüft ein Enum-Element. (Python 3.11+)
  \item \textbf{\pyth{@functools.cache}} Speichert die Ergebnisse von Funktionsaufrufen, um die Ausführung zu beschleunigen (schneller als \pyth{@lru_cache}) (Python 3.9+).
  \item \textbf{\pyth{@functools.cached_property}} Kombination aus \pyth{@property} und \pyth{@cache} für Klassenmethoden (Python 3.8+).
  \item \textbf{\pyth{@functools.cmp_to_key}} Konvertiert eine Vergleichsfunktion in eine Schlüsselfunktion (Python 3.2+).
  \item \textbf{\pyth{@functools.lru_cache}} Speichert die Ergebnisse von Funktionsaufrufen, um die Ausführung zu beschleunigen.
  \item \textbf{\pyth{@functools.partial}} \gquote{Friert} die Argumente einer Funktion ein.
  \item \textbf{\pyth{@functools.partialmethod}} Wie \pyth{partial}, jedoch
  auf Methoden angewendet (Python 3.4+)
  \item \textbf{\pyth{@functools.singledispatch}} Deklariert eine Funktion die den ersten Typ des Arguments überprüft, damit sie mit anderen Funktionen überladen werden kann.
  \item \textbf{\pyth{@functools.singledispatchmethod}} Deklariert eine Methode, die den ersten Typ des Arguments überprüft, damit sie mit anderen Methoden überladen werden kann (Python 3.8+).
  \item \textbf{\pyth{@functools.total_ordering}} Deklariert eine Klasse, die alle Vergleichsoperationen implementiert (Python 3.2+)
  \item \textbf{\pyth{@functools.wraps}} Kopiert die Metadaten einer Funktion auf eine andere Funktion.
  \item \textbf{\pyth{@typing.final}} Deklariert eine finale Klasse oder Methode.
\end{itemize}


\section{Klassifizierung von Decorators}
Decorators können auf verschiedene Arten klassifiziert werden
wie im folgenden beschrieben.

\begin{description}
\item[Implementierungsarten von Decorators] wie Decorators
technisch implementiert werden:

\begin{itemize}
  \item \formaltitle{Funktionsbasierte Decorators.} Decorators, die als Funktionen definiert sind. In der Praxis sind funktionsbasierte Decorators Closures.
  \item \formaltitle{Klassenbasierte Decorators.} Decorators, die als Klassen definiert sind.
\end{itemize}


\item[Aufrufsarten von Decorators] wie Decorators aufgerufen werden:

\begin{itemize}
  \item \formaltitle{Ohne Argumente.} Decorators, die keine zusätzlichen Argumente akzeptieren wie \pyth{@staticmethod}, \pyth{@property}
  \item \formaltitle{Mit Argumenten.} Decorators, die zusätzliche Argumente akzeptieren wie \pyth{@dataclass}, \pyth{@total_ordering}
\end{itemize}

\item[Anwendungsarten von Decorators] auf welchen Typ der Decorator angewendet wird:
\begin{itemize}
  \item \formaltitle{Funktionen.} Ein Decorator, der auf eine Funktion angewendet wird, z.~B.\ \pyth{@staticmethod}, \pyth{@property}
  \item \formaltitle{Klassen.} Ein Decorator, der auf eine Klasse angewendet wird,
  z.~B.\ \pyth{@dataclass}, \pyth{@total_ordering}
\end{itemize}
\end{description}

\section{Allgemeine Struktur eines Decorators ohne Argumente}
Beide Varianten, funktions- oder klassenbasierter Decorator, werden auf die gleiche Weise verwendet. Sie können auf zwei Arten verwendet werden:

\begin{enumerate}
  \item \formaltitle{Manuell} indem du den Decorator einer zu dekorierenden Funktion übergibst
  und das Ergebnis der Dekoration der selben Funktion zuweist:

\begin{python}
my_function = decorator(my_function)
\end{python}

  \item \formaltitle{Mit der \pyth{@}-Syntax} indem du das \pyth{@}-Symbol
  vor den Namen der Decorator-Funktion setzt und die zu dekorierenden
  Funktion in der nächsten Zeile:

\begin{python}
@decorator
def my_function():
    print("Hello, World!")
\end{python}
\end{enumerate}

Die letztere Methode ist die bevorzugte Methode, da sie den Code lesbarer macht.
Intern wird sie jedoch in die erste Methode umgewandelt.

\subsection{Funktionsbasierter Decorator}
Die häufigste und einfachste Form eines funktionsbasierten Decorators.
Ohne die Verarbeitung weiterer Argumente sieht es allgmein wie folgt aus:

\begin{python}
def decorator(func):
    """
    Description of the decorator
    """
    # Preserves the metadata of the original function
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        # Defines the behavior of the decorator
        # Code that runs before the function:
        ...

        # Calls the original function:
        result = func(*args, **kwargs)

        # Code that runs after the function:
        ...

        # Return the result:
        return result

    # Return the wrapper
    return wrapper
\end{python}


\subsection{Klassenbasierter Decorator}
Ein klassenbasierter Decorator ist ein Decorator der als eine Klasse
mit einer \pyth{__call__} Methode implementiert ist. Dies erlaubt ihn
wie einen Funktionsdecorator zu verwenden.

Dieser Ansatz ist hilfreich, wenn man einen Zustand über Funktionsaufrufe hinweg beibehalten muss oder mehr Flexibilität bei der Verwaltung des Verhaltens des Decorators wünscht.

\begin{python}
class decorator:
    """
    Description of the decorator
    """
    def __init__(self, func):
        # Initialize the decorator with the function
        self.func = func

    def __call__(self, *args, **kwargs):
        """Define the behavior of the decorator"""
        # Code that runs before the function:
        ...

        # Call the original function:
        result = self.func(*args, **kwargs)
        
        # Code that runs after the function:
        ...

        # Return the result:
        return result
\end{python}



\section{Allgemeine Struktur eines Decorators mit Argumenten}
Leider ist ein Decorator mit Argumenten etwas komplexer. Hierzu werden
insgesamt drei ineinander verschachtelte Funktionen benötigt:

\begin{itemize}
  \item \formaltitle{Die Decorator-Funktion.} Diese nimmt die Argumente des Decorators entgegen und gibt die innere Funktion zurück.
  \item \formaltitle{Die innere Funktion.} Diese umhüllt die ursprüngliche Funktion und führt die zusätzliche Logik aus.
  \item \formaltitle{Die Wrapper-Funktion.} Diese führt die ursprüngliche Funktion aus und gibt das Ergebnis zurück.
\end{itemize}

Prinzipiell verwendest du den Decorator auf die gleiche Weise wie einen Decorator ohne Argumente. Die Unterschiede liegen wie die Argumente übergeben werden:

\begin{enumerate}
  \item \formaltitle{Manuell} indem du den Decorator mit Argumenten aufrufst und das Ergebnis der selben Funktion zuweist. Achte auf die richtige Reihenfolge der Klammern:

\begin{python}
my_function = (decorator(params))(my_function)
\end{python}
    
  \item \formaltitle{Mit der \pyth{@}-Syntax} indem du das \pyth{@}-Symbol
  vor den Namen der Decorator-Funktion setzt und evlt.\ Argumente
  an den Decorator in Klammern übergibst. Die zu dekorierenden
  Funktion schreibst du in der nächsten Zeile:

\begin{python}
@decorator()
def my_function():
    print("Hello, World!")
\end{python}

  Falls der Decorator Argumente erwartet, musst du die Klammern verwenden:

\begin{python}
@decorator(optional_arg="value")
def my_function():
    print("Hello, World!")
\end{python}

\end{enumerate}


\subsection{Funktionsbasierter Decorator}
\begin{python}
def decorator(func=None, *, optional_arg="default"):
    """
    Description of the decorator
    """
    # Definiere die eigentliche Decorator-Funktion
    def decorator_inner(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            # Defines the behavior of the decorator
            # Code that runs before the function:
            ...

            # Calls the original function:
            result = func(*args, **kwargs)

            # Code that runs after the function:
            ...

            # Return the result:
            return result

        return wrapper

    # If func is not callable, arguments were passed:
    if callable(func):
        return decorator_inner(func)

    # If func is callable, return the inner decorator function
    return decorator_inner
\end{python}

\subsection{Klassenbasierter Decorator}
\begin{python}
class decorator:
    """
    Beschreibung des Decorators
    """
    def __init__(self, func=None, *, optional_arg="default"):
        self.optional_arg = optional_arg
        self.func = func
        # Check if the function is provided at initialization
        if callable(func):
            # If callable, wrap it immediately
            self.wrapper = self._create_wrapper(func)
        else:
            # If not callable, set wrapper to None
            self.wrapper = None

    def _create_wrapper(self, func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
             # Code der vor der Funktion ausgefuehrt wird:
             pass

            # Rufe urspruengliche Funktion auf:
            result = func(*args, **kwargs)

            # Code der nach der Funktion ausgefuehrt wird:
            pass

            # Gebe das Ergebnis zurueck:
            return result

        return wrapper

    def __call__(self, func):
        # If the decorator was called without parentheses,
        # wrap the provided function now.
        if self.wrapper is None:
            return self._create_wrapper(func)
        return self.wrapper
\end{python}


\section{Stapeln von Decorators}
Decorators können \gquote{gestalpelt} werden. Damit meint man, dass man
mehrere Decorators auf eine Funktion angewendet werden.

Der Code wird von oben nach unten gelesen, d.~h.\ der Decorator, der am nächsten an der Funktion steht wird an den Decorator übergeben, der darüber steht.

Dieser Code:

\begin{python}
@bold
@italic
def hello():
    return "Hello, World!"
\end{python}

ist gleichwertig mit diesem Code:

\begin{python}
def hello():
    return "Hello, World!"
hello = italic(bold(hello))
\end{python}

Die beiden Decorators sind wie folgt definiert:

\begin{python}
def htmltag(tag_name):
    def decorator(func):
        @wraps(func)
        def wrapper():
            return f"<{tag_name}>{func()}</{tag_name}>"
        return wrapper
    return decorator

# Define the specific decorators
bold = htmltag("b")
italic = htmltag("i")
\end{python}

Die Ausgabe ist bei beiden Varianten:

\begin{verbatim}
<it><b>Hello, World!</b></it>
\end{verbatim}

Entsprechend umgekehrt ist die Ausgabe, wenn die Reihenfolge der Decorators vertauscht wird.

\section{Beispiel: Timing-Decorator}\label{sec.exa.timing}
In diesem Beispiel wird ein Decorator ohne Argumente definiert, der die Ausführungszeit einer Funktion misst.
Das Ergebnis wird im Closure-Objekt gespeichert.

\begin{python}
import time
from functools import wraps

def timeit(func):
    """
    A decorator that measures and stores the
    execution time of a function.
    
    After executing the decorated function,
    the start time, end time, and duration
    (difference between start and end time) are
    stored in the closure object. These details
    are accessible via the `start_time`,
    `end_time`, and `duration` attributes.

    :param func: The function whose execution time
                 is to be measured.
    :return: The decorated function, which includes
             the timing details.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        # Store start time
        wrapper.start_time = time.time()

        # Call the decorated function
        result = func(*args, **kwargs)

        # Store end time and calculate duration
        wrapper.end_time = time.time()
        wrapper.duration = (
            wrapper.end_time - wrapper.start_time
        )
        return result

    # Initialize timing attributes to None before
    # the function is called
    wrapper.start_time = None
    wrapper.end_time = None
    wrapper.duration = None

    return wrapper
\end{python}

Der Decorator kann wie folgt verwendet werden:

\begin{python}
@timing
def example_function():
    time.sleep(1)

example_function()
print(example_function.start_time)
print(example_function.end_time)
print(example_function.duration) # 1.0000...
\end{python}

Der \pyth{timeit}-Decorator dekoriert die Funktion \pyth{example_function}.
Der Decorator umhüllt die Funktion und misst die Ausführungszeit beim Aufruf.
Die Ergebnisse werden in den Attributen \pyth{start_time}, \pyth{end_time} und \pyth{duration} gespeichert. Nachdem die Funktion aufgerufen wurde, können diese Werte abgerufen werden.


\section{Beispiel: Timing-Decorator mit Messmethode}
In Abschnitt~\ref{sec.exa.timing} wurde ein einfacher Timing-Decorator gezeigt.
In diesem Beispiel wird ein Timing-Decorator mit einem Argument gezeigt, der die Messmethode (z.~B.\ \pyth{time.time()} oder \pyth{time.perf_counter()}) als Argument akzeptiert.

\begin{python}
import time
from functools import wraps


def timeit(func, *, method='time'):
    """
    A decorator that measures and stores the
    execution time of a function.

    :param method: The method used to measure time.
                   Can be 'time' or 'perf_counter'.
    :return: The decorated function, which includes the
             timing details.
    :raises ValueError: If an invalid value in method is provided.
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # Choose the timer function based on the method
            if method == 'time':
                timer = time.time
            elif method == 'perf_counter':
                timer = time.perf_counter
            else:
                raise ValueError(
                    "Invalid value in 'method'."
                    "Use 'time' or 'perf_counter'."
                )

            # Store start time
            wrapper.start_time = timer()
            result = func(*args, **kwargs)
            wrapper.end_time = timer()
            wrapper.duration = (
                wrapper.end_time - wrapper.start_time
            )
            return result

        # Store timing attributes in the wrapper
        wrapper.start_time = None
        wrapper.end_time = None
        wrapper.duration = None

        return wrapper

    if callable(func):
        return decorator(func)
    return decorator
\end{python}


\section{Empfehlungen aus der Praxis}
Die folgende Liste zeigt einige Empfehlungen und bewährte Vorgehensweisen für die Verwendung von Decorators:

\begin{description}
\item[Verwende functools.wraps] Um die Metadaten wie Funktionsname Docstring u.~a.\ der ursprünglichen Funktion zu bewahren, verwende \pyth{@wraps} in deinem Decorator.
Ohne \pyth{@wraps} kann die dekorierte Funktion ihren Namen und
Docstring verlieren.

\item[Fokussiere deinen Decorator auf eine Aufgabe] Ein Decorator sollte nur \emph{eine} Aufgabe erfüllen. Wenn ein Decorator mehrere Aufgaben erfüllt, wird der Code schwerer zu lesen und zu warten.

\item[Verwende aussagekräftige Namen] Benenne deine Decorators so, dass der Zweck klar ist. Wähle einen beschreibenden Namen der die Absicht des Decorators
sofort erkennen lässt.
Für interne Funktionen, wähle ein Suffix oder Präfix \pyth{decorator} oder \pyth{wrapper}. Dies ist hilfreich beim Debuggen.

\item[Kombiniere mehre Decorators vorsichtig] Wenn du mehrere Decorators kombinierst, achte auf die Reihenfolge, da sie sich gegenseitig beeinflussen
können.

\item[Halte die Anzahl der Parametern gering] Wenn dein Decorator Argumente akzeptiert, beschränke die Parameteranzahl auf das Wesentliche. Zu viele Parameter können die Lesbarkeit beeinträchtigen und die Anwendung des Decorators erschweren.

\item[Erzwinge Parameternamen bei parametrisierten Decorators] 
Wenn dein Decorator Argumente akzeptiert, sorge für eine klare Lesbarkeit, indem du die Verwendung von Parameternamen erzwingst. Dies erreichst du durch ein Asterisk (\pyth{*}) in der Funktionssignatur des Decorators.
Ein Beispiel hierfür ist \pyth{@dataclasses.dataclass}.

\item[Nutze klassenbasierte Decorators für komplexe Fälle] Für komplexe Decorators mit Zustandsverwaltung oder zusätzlichen Attributen kann ein klassenbasierter Decorator übersichtlicher sein als eine Funktion.
Zudem kannst du den Zustand über mehrere Funktionsaufrufe hinweg beibehalten.

\item[Erlaube optionale Klammern für deinen Decorator] Falls dein Decorator
optionale Argumente unterstützt, sollte er sowohl mit als auch ohne Klammern verwendet werden können. Dadurch wird die Anwendung vereinfacht und potenzielle Fehler durch vergessene Klammern werden vermieden

\item[Teste deinen Decorator] Teste deinen Decorator genauso wie du Funktionen testest. Überprüfe, ob es sich wie erwartet verhält und ob es korrekt mit anderen Decorators und Funktionen interargiert.
\end{description}


\begin{thebibliography}{10}
    % Sort lastname author
\bibitem{pythonsimplified.legb-rule}
    \bauthor{Chetan Ambi},
    \btitle{Understanding namespaces and scope in Python (2021)},
    \url{https://pythonsimplified.com/understanding-namespaces-and-scope-in-python/}

\bibitem{beartype}
    \btitle{Beartype},
    \url{https://github.com/beartype/beartype}

\bibitem{enforce.py}
    \bauthor{RussBaz},
    \btitle{Enforce},
    \url{https://github.com/RussBaz/enforce}

\bibitem{lord63-awesome-python-decorator}
    \btitle{Awesome Python Decorator},
    \url{https://github.com/lord63/awesome-python-decorator}

\bibitem{mccullum}
  \bauthor{Nick McCullum},
  \btitle{The Complete Guide to Python Decorators (Oct.~2020)},
  \url{https://www.nickmccullum.com/complete-guide-python-decorators/}

\bibitem{realpython.closures}
    \bauthor{Leodanis Pozo Ramos},
    \btitle{Python Closures: Common Use Cases and Examples (2024)},
    \url{https://realpython.com/python-closures/}

\bibitem{realpython.primer-decorators2024}
    \bauthor{Geir Arne Hjelle},
    \btitle{Primer on Python Decorators (2024)},
    \url{https://realpython.com/primer-on-python-decorators/}

\bibitem{pydantic}
  \btitle{Pydantic},
  \url{https://docs.pydantic.dev/}

\bibitem{typeguard}
    \btitle{Typeguard},
    \url{https://github.com/agronholm/typeguard}
\end{thebibliography}
\end{multicols}
\end{document}